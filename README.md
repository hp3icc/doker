# Doker Amateur Radio images Proyect

# Info

Collection of images of docker containers for different applications for amateur radio use.

All docker-compose.yml files are in network bridge, edit and configure network according to your needs.

#

# list of containers included:

* HBLINK3 (Cortney N0MJS, Tomasz SP2ONG, Steve N4IRS)

* ADN peer DMR servr (simple version without self-service and dashboard by Bruno CS8ABG)

* IPSC Repeater to MMDVM (version dual network)

* P25Reflector with Dashboard (Shane M0VUB)

* XLXD reflector v2.5.x dual Dashboard (Jean LX3JL)

* DVSwitch Server (Andy Taylor MW0MWZ , STEVEN N4IRS, dvswitch.groups.io)

* NXDN2DMR (Andy CA6JAU)

* DMR2DMR (Andy CA6JAU)

* FCS2DMR (Jonathan Naylor G4KLX)

* YSF2DMR (Andy CA6JAU)

* YSF2YSF Bridge (dgid compatible by Antonio IU5JAE)

* pYSFReflecto3 (with bridge DMR and APRS gateway by Antonio IU5JAE)

#

# Install

If you do not have docker installed on your computer, you can execute the following steps:

    apt update

    apt upgrade -y

    apt install sudo curl -y

    bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/docker.sh)"

#

# Image containers

To select the different docker images you just have to change branch.

<img src="https://gitlab.com/hp3icc/doker/-/raw/main/Screenshot_1.png" width="550" height="450">

#

It is possible to mix more than one project in a single docker-compose.yml file

success in your projects.

HP3ICC
73.
